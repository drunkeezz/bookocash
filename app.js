const MongoClient = require('mongodb').MongoClient;
const assert = require ('assert');
const mongoose = require('mongoose');

//connection url
const url = 'mongodb://127.0.0.1:27017';

//database name
const dbName = 'BookOCash';

MongoClient.connect(url,{useNewUrlParser: true , useUnifiedTopology:true} ,function(err, client){
    assert.strictEqual(null, err);
    console.log("connected successfully to server");

    const db = client.db(dbName);
    client.close();

})

